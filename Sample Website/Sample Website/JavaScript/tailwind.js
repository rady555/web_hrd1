tailwind.config = {
    theme: {
      extend: {
        colors: {
          mine: "#FF4500",
        },
        fontFamily: {
          popin: ["Poppins", "sans-serif"],
        },
        screens: {
          sm: "640px",
          // => @media (min-width: 640px) { ... }
  
          md: "768px",
          // => @media (min-width: 768px) { ... }
  
          laptop : "1023px",
          // => @media (min-width: 1024px) { ... }
  
          xl: "1280px",
          // => @media (min-width: 1280px) { ... }
  
          "2xl": "1520px",
          // => @media (min-width: 1536px) { ... }
        },
      },
    },
  };
  