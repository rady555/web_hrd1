// function myFun(){
//     document.getElementById("demo").innerHTML="JavaScript"
// }
//Sread operator
function sum(a,b,c,d){
    return a+b+c+d;
}

const number = [2,4,6,8];
console.log(sum(...number));

//iteral object
let arr1 = [1,2,3];
let arr2 = [4,5,6];
    // arr = arr1.concat(arr2);
    arr3 = [...arr1,...arr2];
console.log(arr3);

const arr4 = ["kong","rady"];
const [fname,lname] = arr4;
console.log(fname);
console.log(lname);

const [name1,name2] = "Rady Kong".split(" ")
console.log(name1);
console.log(name2);

const [,fname1,,,,lname2] = ["Rady","Krya","Sithov","Leakena","sophat","koko"] 
console.log("fname:", fname1)
console.log("lname:", lname2)

const [arr6, arr9] = "abc"
console.log(arr6)
console.log(arr9)

let person = {};
[person.name, person.nationality] = "Bopha Khmer".split(' ')
console.log(person.name, person.nationality)

let user = {
    name : '',
    country: ''
};
user = "Kiko Thai".split(' ')
console.log(user.name , user.country)


let book ={
	title: "titanic",
	realeasedate: "01/02/2012",
}
let {title,realeasedate} = book
console.log(title,realeasedate)
