import React from 'react'

export default function TableComponent({person}) {

    return (
      <div className="w-5/6">
        <table className="table-full w-full m-auto text-xl my-20 text-center">
            <thead >
                <tr className="">
                    <th className="p-3 px-10 w-32 font-semibold ">ID</th>
                    <th className=" p-3 uppercase w-60 font-semibold">Email</th>
                    <th className=" p-3 uppercase w-60 font-semibold">Username</th>
                    <th className=" p-3 uppercase w-48 font-semibold">Age</th>
                </tr>
            </thead>
            <tbody>
                {person.map((item)=>(
                    <tr key={item.id} className="text-xl">
                        <td className=" p-3">{item.id }</td>
                        <td className=" p-3">{item.email === ""?"null" : item.email}</td>
                        <td className=" p-3">{item.username=== ""?"null" : item.username}</td>
                        <td className=" p-3">{item.age  === ""?"null" : item.age}</td>
                    </tr>
                ))} 
            </tbody>
        </table>
      </div>
    )
  }

