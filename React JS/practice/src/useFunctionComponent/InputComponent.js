import React, { useState } from 'react'
import TableComponent from './TableComponent';

export default function InputComponent({person,setPerson}) {

    const [newPer, setNewPer] = useState({});

    const handleChange = (e) => {
        const {name,value} = e.target;
        setNewPer({...newPer, [name]:value});
    }

    const handleSubmit = () => {
        setPerson([...person, {...newPer, id: person.length +1}]);
    }

  return (
        <div className="bg-purple-300 w-full h-full">
            <div className="flex flex-col justify-center items-center ">           
                <div className="w-5/6 mt-16">
                    <p className="text-center text-5xl font-bold "><span className="text-5xl font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-purple-600 via-pink-600 to-blue-600">Please fill your</span> information</p>                
                
                    {/* input email */}
                    <label for="email-address-icon" class="block mb-2 text-xl font-medium text-gray-900 mt-5">Your Email</label>
                    <div class="relative">
                        <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                            <svg aria-hidden="true" class="w-5 h-5 text-blue-500 " fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path><path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path></svg>
                        </div>
                        <input onChange={handleChange} name="email" type="text" id="email" class="bg-gray-50 border border-gray-300 text-gray-900 text-xl rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5 " placeholder="name@gmail.com" />
                    </div>

                    {/* input username */}
                    <label for="website-admin" class="block mb-2 text-xl font-medium text-gray-900 mt-5">Username</label>
                    <div class="flex">
                        <span class="inline-flex items-center px-3 text-xl text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md ">
                            @
                        </span>
                        <input onChange={handleChange} type="text" name="username" id="username" class="rounded-none rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-xl p-2.5 " placeholder="Kong Rady" />
                    </div>

                    {/* input age */}
                    <label for="" class="block mb-2 text-xl font-medium text-gray-900 mt-5">Age</label>
                    <div class="flex">
                        <span class="inline-flex items-center px-3 text-xl text-red-600 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md ">
                            ❤
                        </span>
                        <input onChange={handleChange} type="text" name="age" id="age" class="rounded-none rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-xl p-2.5 " placeholder="21" />
                    </div>
                </div>

                {/* Button Register  */}
                <div class="rounded-md hover:bg-none bg-gradient-to-r from-green-500 via-purple-500 to-red-500 p-[2px] mt-10">
                    <div class="flex items-center justify-center">
                        <button onClick={handleSubmit} class="bg-white text-xl hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-36 hover:shadow hover:bg-gradient-to-r from-orange-500 via-green-500 to-pink-500 rounded">Register</button>                
                    </div>
                </div>

                {/* set props */}
                <TableComponent person={person} setPerson={setPerson} />
            </div> 

    </div>
  )
}
