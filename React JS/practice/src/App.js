
import { useState } from 'react';
import InputComponent from './useFunctionComponent/InputComponent';

 export default function App() {
  const [person, setPerson] = useState(
   [ {
        id: 1,
        email: 'maradykong@example.com',
        username: 'MiMaMa',
        age: 18,
    }]
  )
  return (
    <div className='App'>
      <InputComponent person={person} setPerson={setPerson}/>     
    </div>
  );
}