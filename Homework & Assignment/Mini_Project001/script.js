
tailwind.config = {
    theme: {
      extend: {
        colors: {
            white_f: "#f1f5f9",
            pink: "#334155",
            white_fff: "#f1f5f9",
            gray: "#475569",
            dtbase:"#f5f5f5",
            bgkey:"#f5f3ff",
            bgstar:"#fff1f2",
            about_bg:"#fffbeb",
            hr: "#e2e8f0",
            border:"#e2e8f0",
            link:"#1e3a8a",
        },
        fontFamily: {
          noto: ['Noto Sans Khmer','sans-serif'],
        },
        backgroundImage:{
          'bg_tech': "url('img/tech-1.jpg')"
        },
        screens: {
          sm: "640px",
          // => @media (min-width: 640px) { ... }
  
          md: "768px",
          // => @media (min-width: 768px) { ... }
  
          laptop : "1023px",
          // => @media (min-width: 1024px) { ... }
  
          xl: "1280px",
          // => @media (min-width: 1280px) { ... }
  
          "2xl": "1520px",
          // => @media (min-width: 1536px) { ... }
        },
      },
    },
  };
  
